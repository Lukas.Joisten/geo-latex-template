<!---
                  Copyright Lukas Joisten
 Distributed under the Boost Software License, Version 1.0.
       (See accompanying file LICENCE.txt or copy at
          https://www.boost.org/LICENSE_1_0.txt)
--->

# Template für Hausarbeiten in der Geographie an der RWTH

**Das PDF dieses Templates wird automatisch generiert und befindet sich [hier](https://gitlab.com/Lukas-J/geo-latex-template/-/jobs/artifacts/master/raw/template.pdf?job=build).**

Dieses Template darf gerne frei verwendet, verbreitet und auch modifiziert werden.

Es ist angelehnt an die Vorgaben aus https://www.geographie.rwth-aachen.de/cms/Geographie/Das-Institut/Ausstattung/Bibliothek/~pwgf/Anleitung-Erstellung-einer-wissenschaft/ (Stand Februar 2021)

# LaTeX Tipps und Tricks

## Warum LaTeX
LaTeX ist ein Textsatzsystem, welches aus reinem Text u. a. ein PDF-Dokument erzeugt. 
Für wissenschaftliche Arbeiten ist LaTeX auch von Anfang an ausgelegt: Das Inhaltsverzeichnis und Literaturverzeichnis werden z.B. automatisch erzeugt. Außerdem lassen sich unglaublich gut Formeln damit schreiben, weil der eingebaute "Formeleditor" sehr mächtig ist. In vielen wissenschaftlichen Disziplinen ist LaTeX der Standard um wissenschaftliche Texte zu schreiben.

Im Gegensatz zu Programmen wie Word, muss man sich dabei nur einmal mit dem Layout beschäftigen und dann nicht wieder. Da ich das für euch schon zum Großteil erledigt habe, müsst ihr hier fast nichts mehr tun. Und wenn ihr LaTeX auch nicht mit Nachdruck davon überzeugt, könnt ihr auch nicht gegen die Formatvorgaben verstoßen.

Deshalb müsst ihr euch nur noch um das Wesentliche eurer Arbeit kümmern, euren Text.

Falls ich euch noch nicht ganz überzeugt habe, schaut gerne hier, warum Ihr LaTeX nutzen solltet: https://latex.tugraz.at/latex/warum

Und falls ihr noch nicht ganz verstanden, was LaTeX überhaupt ist, schaut gerne hier rein: <https://de.wikibooks.org/wiki/LaTeX-Kompendium:_Schnellkurs:_LaTeX_%E2%80%93_Was_ist_das%3F>

## Einsteiger Seiten
Da ihr wahrscheinlich euch gerade zum ersten Mal mit LaTeX beschäftigt, habe ich euch eine kleine Linksammlung zusammengestellt, die den Einstieg erleichtern soll.

https://de.wikibooks.org/wiki/LaTeX-Kompendium

https://www.heise.de/download/blog/Einfuehrung-in-LaTeX-3599742

https://latex.tugraz.at/start

## Software Setup
Um LaTeX nutzen zu können, braucht ihr zum einen eine Distribution und zum anderen einen Editor. Die Distribution beinhaltet u. a. LaTeX selbst sowie einen Paketmanager um Erweiterungen (\usepackage) einzubinden. Der Editor ist das Programm, mit dem ihr LaTeX bedient. Es nimmt einem Arbeit ab, indem es die Kompilierung und Syntaxhervorhebung, sowie einige nützliche Shortcuts übernimmt.

**Um den Einstieg zu erleichtern, empfehle ich den Online-Editor https://www.overleaf.com/.** Dort könnt ihr das [Template](https://www.overleaf.com/latex/templates/geo-latex-template/wxkytdjbpqbs) mit einem Knopfdruck verwenden.
Hier wird euch das ganze Software Setup abgenommen und ihr könnt zusammen in Echtzeit an einem Dokument arbeiten, wie das z.B. bei Google Docs möglich ist.

Wenn ihr aber am Desktop arbeiten möchtet oder ihr der Internetleitung nicht so ganz vertraut, müsst ihr euch für eine Distribution und einen Editor entscheiden. Als eine gute LaTeX-Distribution für Windows kann ich MiKTeX empfehlen, für Mac OS empfiehlt sich MacTeX. Linux-User sollten sich in ihren Paketquellen nach einer guten Distribution umsehen. Allgemein empfiehlt sich aber auch die Distribution TeX Live.
Eine Übersicht mit Downloadlinks findet sich hier: https://www.latex-project.org/get/

Als guten Editor empfehle ich TeXstudio (https://www.texstudio.org/).

**Der Vorteil am Arbeiten am Desktop ist, dass man mit der Versionierungssoftware Git, sein Projekt versionieren kann.** Im Zusammenspiel mit einem Server, wie z.B. https://git.rwth-aachen.de/ erstellt man so eine Sicherungskopie. Tutorials, wie man in Git einsteigen kann, finden sich im Internet. Ich empfehle, Git zusammen mit TortoiseGit (https://tortoisegit.org/) zu benutzen, da diese grafische Oberfläche einem die Shell-Befehle erspart.

Bei LaTeX ist aber Vieles Geschmackssache. Deshalb hier noch eine Übersicht an Programmen und Editoren: https://latex.tugraz.at/programme/plattformunabhaengig

## Wie funktioniert das mit dem Zitieren?
Um eine Quelle zitieren zu können, muss sie erstmal der Literaturdatenbank hinzugefügt werden. Hier heißt die Datei ``literatur.bib``. Die Einträge sind dabei im BibTeX-Format angelegt. 

Aber woher bekommt man nun die BibTeX-Einträge? Mit u. a. folgenden Literaturdatenbanken kann man sich BibTeX-Einträge exportieren lassen:
- https://scholar.google.de/
- https://apps.webofknowledge.com
- https://www.scopus.com/
- https://www.crossref.org/
- https://www.researchgate.net/

Literaturverwaltungsprogramme, wie Citavi, EndNote und Zotero können auch solche BibTeX-Einträge anlegen. 
Ich habe mit Citavi gute Erfahrungen gemacht. Man kann Citavi z.B. anhand der ISBN oder DOI nach Literatur suchen lassen kann. Dadurch kann Citavi die BibTeX-Einträge selbst erstellen und im Anschluss in die Literaturdatenbank exportieren. Eine Anleitung dazu findet ihr hier: https://www1.citavi.com/sub/manual6/de/index.html?101_creating_a_publication_with_latex.html

Eine Anleitung für EndNote findet ihr hier: https://unimelb.libguides.com/latexbibtex/endnote

Eine Anleitung für Zotero findet ihr hier: https://unimelb.libguides.com/latexbibtex/zotero

Wie innerhalb eures Texts zitiert, könnt ihr euch im Template anhand von Beispielen angucken. Im Wesentlichen sieht das aber so aus: ``\parencite[ggf. Seitenzahl]{BibTeX-Key}`` oder ``\textcite[ggf. Seitenzahl]{BibTeX-Key}``.

## Hilfe, wie mache ich ...
Da hilft, wie in den meisten Fällen, im Internet nach der gewollten Funktion oder dem Problem zu suchen. Am besten fügt ihr am Anfang direkt "Latex ..." ein. Die einfachsten Funktionen lassen sich noch auf Deutsch finden, aber sobald das Problem etwas komplizierter wird, findet ihr die Lösung oft nur auf Englisch. Häufig sind auch Foren wie <https://tex.stackexchange.com> hilfreich, weil ihr sicherlich nicht die Ersten seid, die das Problem haben.

## Mir ist das alles zu kompliziert ...
Ja, in LaTeX ist die Einstiegshürde vergleichsweise hoch. Wenn man sich aber einmal eingearbeitet hat, will man es für wissenschaftliche Arbeiten nicht mehr missen. Vor allem das automatische Literaturverzeichnis sowie die Möglichkeit, Formeln einzugeben sind ultra praktisch.

Natürlich könnt ihr auch weiterhin Word oder LibreOffice benutzen. LaTeX macht aber nach dem Einstieg das Leben doch viel einfacher. Vor allem kann man, wenn man einmal sein Template hat, das für alle möglichen Projekte benutzten und erweitern.

# Abschlussarbeiten
In Abschlussarbeiten wollt ihr euer Dokument wahrscheinlich zweiseitig setzen. Das heißt, alle neuen Kapitel starten auf einer rechten Seite, dazu gehört auch das Inhaltsverzeichnis.

Um das zu umzusetzen, solltet ihr lieber die Klasse `scrreprt` mit den zusätzlichen Optionen `twoside, open=right` in `pre-preamble.tex` verwenden. In `preamble.tex` ersetzt ihr 

```latex
\RedeclareSectionCommands[beforeskip=-2\baselineskip, afterskip=1\baselineskip]{section, subsection, subsubsection} % Abstände vor und nach Überschriften festlegen
```

mit

```latex
\RedeclareSectionCommands[beforeskip=-2\baselineskip, afterskip=1\baselineskip]{chapter, section, subsection, subsubsection} % Abstände vor und nach Überschriften festlegen
\usepackage{scrlayer-scrpage}
\ofoot*{}
\cfoot*{\pagemark}
```

Direkt nach `\pagenumbering{arabic}` fügt ihr in `template.tex` ein `\cleardoubleoddpage` ein.

Außerdem ist nun das höchste Kapitelkommando `\chapter` anstelle von `\section`. Nach `\chapter` folgt `\section` und wie gewohnt `\subection` und `\subsubsection`. 

Siehe https://git.rwth-aachen.de/Lukas.Joisten/geo-latex-template/-/merge_requests/4/diffs für ein Code-Beispiel.

## Externes Deckblatt verwenden
Es kann vorkommen, dass ihr von eurem Lehrstuhl ein vorgeschriebenes Deckblatt verwenden sollt. 

In dem Fall, füllt die Deckblattvorlage in Word aus, [speichert das Deckblatt als PDF ab](https://support.microsoft.com/de-de/office/speichern-oder-konvertieren-in-pdf-oder-xps-in-office-desktop-apps-d85416c5-7d77-4fd6-a216-6f4bf7c7c110) und legt es im Ordner [`Einzubindende_PDFs`](Einzubindende_PDFs) ab. 

Dann ändert ihr die Boolean Variable `ExternalDeckblatt` in [`template.tex`](template.tex) von "false" auf "true". Im `includepdf` Befehl ändert ihr den Pfad zu eurem generierten PDF. Und schon wird das generierte PDF als Deckblatt eingebunden.

# Verbesserungsvorschläge

Über Verbesserungsvorschläge freue ich mich :)
Erstellt ein [Merge Request](https://git.rwth-aachen.de/Lukas.Joisten/geo-latex-template/-/merge_requests), wenn ihr selber was am Template geändert habt und es mit der Welt teilen wollt. Bei Bugs oder Verbesserungsvorschlägen legt bitte ein [Issue](https://git.rwth-aachen.de/Lukas.Joisten/geo-latex-template/-/issues) an.

# Disclaimer
**Ich beanspruche keine Richtigkeit oder Vollständigkeit.** Jede und jeder sollte selbst überprüfen, ob das Template den eigenen Vorgaben entspricht. 
